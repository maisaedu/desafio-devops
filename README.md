Aqui estão os desafios para a vagas de **DevOps**.

Não há diferença de testes para diferentes níveis de profissionais, porém ambos os testes serão avaliados com diferentes critérios, dependendo do perfil da vaga.

Vale reforçar, que a solução deve contemplar os testes de **Terraform/IaC** e **Kubernetes**.


## Como entregar estes desafios
Você deve realizar o _**fork**_ este projeto e fazer o **_push_** no seu próprio repositório e enviar o link como resposta ao recrutador que lhe enviou o teste.

A implementação deve ficar na pasta correspondente ao desafio. Fique à vontade para adicionar qualquer tipo de conteúdo que julgue útil ao projeto, alterar/acrescentar um README com instruções de como executá-lo, etc.

**Obs.**:
- Você não deve fazer um Pull Request para este projeto!

### Extras

- Descreva o processo de resolução dos desafios;
- Descreva como utilizar a sua solução;
- Sempre considerar melhores práticas como se fosse um ambiente de produção;
